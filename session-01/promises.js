//this file won't run - its for a discussion only
///(it depends on non-existing libs)

const { assign  } = Object
const darDal = require('dal/dar')
const osherDal = require('dal/osher')


const regular = () => {
    const results = {};

    const prom1 = darDal.getById(23)
    .then(obj =>
        assign(results, { obj }) &&
        Promise.all([
            () =>  darDal.getCollections(obj.friendsIds),
            () =>  darDal.getHobbiesDescriptors(obj.friendsIds),
        ])
    )
    .then(([collections, hobbies]) => 
        assign(results, { hobbies, collections})
        && console.log(results)
    )

    console.log('prom1 sent');

    const prom2 = osherDal.getJsExperty().then(refactorCode)

    return [prom1, prom2]
}

const asyncF = async () => {
    const obj = await darDal.getById(23);

    const [collections, hobbies] = await Promise.all([
        () =>  darDal.getCollections(obj.friendsIds),
        () =>  darDal.getHobbiesDescriptors(obj.friendsIds),
    ]);

    const results  = { obj, collections, hobbies };

    console.log(results)
}

function refactorCode() {
    //....
}