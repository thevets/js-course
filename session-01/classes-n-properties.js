class Person {
    constructor(name) {
        this.name = name
    }
    hello() {
         return `hello, I'm ${this.name}` 
    }
    greet() {
        return `I ${this.name}, greeting you` 
    }
}

function PersonOldStyle(name) {
    this.name = name
}
Object.defineProperties(PersonOldStyle, {
    hello: { 
        enumerable: false,
        value: function() { `hello, I'm ${this.name}`  },
    },
    greet: { 
        enumerable: false,
        value: function() { `I ${this.name}, greet you`  },
    }
});



class Employee extends Person() {
    constructor(name, jobTitle) {
        super(name)
        this.jobTitle = jobTitle;
    }
    work() { 
        return 'chop chop chop'
    }
    greet() {
        return `${super.greet()}, as a professional ${this.job}`
    }
}


function PersonFactory1(name) {
    return new Person(name)
}

module.exports = function PersonFactory2(name) {
    return Object.defineProperties(
        { 
            name
        },
        {
            hello: nonEnumerableDescr(hello),
            greet: nonEnumerableDescr(greet)
        }
    )
}

function nonEnumerableDescr(value) {
    return { enumerable: false, value }
}

Object.defineProperty({}, 'name', {
    configurable: true, 
    enumerable: false,
    get: function(){ return this.$name },
    set: function(name) { this.$name = name },
});

Object.defineProperty({}, 'name', {
    configurable: true, 
    enumerable: false,
    value: function(){ return this },
    writable: false,
});

o = {}
o.name = 'osher';


Object.defineProperty({}, 'name', {
    configurable: true, 
    enumerable: true,
    value: 'osher',
    writable: true,
});




function hello() { `hello, I'm ${this.name}`  }

function greet() { `I ${this.name}, greet you`  }



function getIt() { return this }
const  o  = { getIt, that: 'is it '};



const o1 = { getIt, that: 'is it ', nested: { pull() { return getIt() } } };




