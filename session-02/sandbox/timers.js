const fs = require('fs');

setTimeout(() => hello(1));
setTimeout(() => hello(2), 500);
setTimeout(() => hello(3));
setTimeout(() => hello(4), 1500);

fs.readFile('./timers.js', (err, buff) => {
    setTimeout(() => console.log('buff arrived', buff+"")), 500;
})

setTimeout(() => setTimeout(() => hello(5), 500));


function hello(a) {
    console.log('hello', a);
}