
function hello(a) {
    console.log('exec-hello', a);
    setImmediate(() => console.log('set immediate', a));
    setTimeout(() => console.log('set timeout', a));
    process.nextTick(() => console.log('nextTick', a));
}

hello('global');
setTimeout(() => hello('timeout'));
console.log('mehaglobal');

/*
output:
=================
exec-hello global
mehaglobal
nextTick global
set timeout global
exec-hello timeout
nextTick timeout
set immediate global
set immediate timeout
set timeout timeout

*/

/*const { EventEmitter } = require('events');


module.exports = (config) => {
    const e = new EventEmitter();

    //do some privsate stuff...

    process.nextTick(() => {
        e.emit('init');
    })

    return e;
}


///---------------
const factory = require('...')
const config = require('config-reader....')
const controller = require(...EventEmitter.)

const mgr = factroy(config)
mgr.on('init', () => {
    controller.setMgr(mgr)
});
    
})

*/