/*
logger config:
  name
  levels:
    default:      info
    add:           debug
    lig/cmd/fib:   warn
    lib/exec:      info
*/
module.exports = ({
  name: app,
  levels: { default: defaultLevel, ...levels },
  pino = require('pino'),
}) => {
  const mainLogger = pino({ app });
  mainLogger.level = levels.calc;
  mainLogger.of = of;
  return mainLogger;

  function of(name) {
    const child = this.child({ module: name });
    child.level = levels[name] || defaultLevel;
    child.of = of;
    return child;
  }
}

