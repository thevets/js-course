const { PORT = 3000 } = process.env;

require("http").createServer(({ url, method }, res) => {
    console.log('http, %j', { url, method });
    res.end("ok");
}).listen(PORT, err => {
    if (err) {
        console.log('could not start service', err.message, { ...err });
        process.exit(1);
        return;
    }

    console.log('started OK on port: %s', PORT);
});
