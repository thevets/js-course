FROM node:16-alpine AS build

COPY ./package.json ./
RUN npm install


FROM alpine

RUN apk add \
    --repository http://dl-cdn.alpinelinux.org/alpine/v3.13/main/ \
    --no-cache \
    nodejs=14.19.0-r0

WORKDIR /opt/calc

COPY --from=build node_modules node_modules
COPY ./bin ./bin
COPY ./config ./config
COPY ./lib ./lib
COPY ./package.json ./

ENTRYPOINT ["node", "bin/calc"]
CMD ["-h"]