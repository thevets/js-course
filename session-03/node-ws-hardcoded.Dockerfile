FROM node:16-alpine

RUN echo 'require("http").createServer((q,a) => a.end("ok")).listen(process.env.PORT || 3000, e => console.log(e?.message || "started OK"))' > index.js

CMD ["node", "index.js"]
