FROM node:16-alpine

WORKDIR /opt/calc

COPY ./bin ./bin
COPY ./config ./config
COPY ./lib ./lib
COPY ./package.json ./

RUN npm install

ENTRYPOINT ["node", "bin/calc"]
CMD ["-h"]